import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import uproot
import os
import awkward as ak
import betsee.itsdaq as itsdaq
import betsee.bitstream as bs
import betsee.enc8b10b as enc8b10b
import argparse

S2NS = 1000000000

def prepare_plot(ylabel = '', bits = False):
    if bits:
        plt.xlabel('Time (arb.)')
        plt.ylabel('Bit')
    else:
        plt.xlabel('Elapsed time [s]')
        plt.ylabel(ylabel)
        plt.legend()
        plt.ylim(bottom=0)

def bits_plot(y, regname = ''):
    list_of_timeseries = []
    for bit in range(32):
        num = 1 << bit
        list_of_timeseries.append( (y & num) == num )
    plt.matshow(list_of_timeseries, aspect = 'auto')
    plt.title(regname)

def make_plots(res_dict, namelist, file_list, prefix, splice, cumulative=False, bits=False, erase_offset=False):
    if splice:
        os.makedirs(prefix+'_split/', exist_ok = True)
        for i, file_name in enumerate(file_list):
            plt.figure()
            fname = os.path.basename( os.path.splitext(file_name)[0] )
            for name in namelist:
                counts, times, splits = res_dict[name]
                start = ak.sum(splits[:i])
                end   = start + splits[i]
                data = counts[start:end]
                if erase_offset:
                    data = data - data[0]
                if bits:
                    bits_plot(data,name+'_'+fname)
                else:
                    plt.plot(times[start:end], data, label=name)
            prepare_plot(bits=bits)
            plt.title(fname)
            plt.savefig(prefix+'_split/'+fname+'.png')
            plt.close()

    plt.figure()
    for name in namelist:
        counts, times, splits = resdir[name]
        if bits:
            bits_plot(counts,name)
        else:
            plt.plot(times, counts, label=name)
    prepare_plot(bits=bits)
    plt.savefig(prefix+'.png')
    plt.close()

    if cumulative:
        plt.figure()
        for name in namelist:
            counts, times, splits = resdir[name]
            cumul = counter_cumulative(counts)
            plt.plot(times, cumul, label=name)
        prepare_plot()
        plt.savefig(prefix+'_cumulative.png')
        plt.close()

def count_bits(arr):
    out = ak.zeros_like(arr)
    for bit in range(32):
        num = 1 << bit
        out = out + ( (arr & num) == num )
    return out

def count_SEUs(seu_reg):
    #Check for at least on SEU RR
    if ak.count(seu_reg) == 0:
        return ak.Array([0])
    #Get first SEU RR
    golden = ak.flatten(seu_reg)[0]
    #Find bits that flipped
    flipped = ak.firsts(seu_reg) ^ golden
    #Count them
    return count_bits(flipped)

def get_packets_from_cap(cap):
    #Digest the stream into packets of 8b words
    acap=enc8b10b.align(cap)
    data,k=enc8b10b.decode(acap)
    mydata,myk=itsdaq.remove_idles(data,k)
    packets, evtmask = itsdaq.find_packets(mydata,myk)
    #packets=ak.flatten(packets)
    typ=itsdaq.packet_typ(packets)
    return packets, typ, evtmask

#Use case: have an array of e.g. timestamps from good events, need to match them to packets
def get_broadcasted_flattened(array, packet_shape):
    _, array = ak.broadcast_arrays(packet_shape, array)
    return ak.flatten(array)

#Assume rr_vals is an ak array with periodic resets to 0
def counter_cumulative(rr_vals):
    #indices of events right before overflow/reset
    rr_vals = rr_vals.to_numpy()
    drops = np.append(rr_vals[1:] < rr_vals[0:-1], False)
    drop_steps = np.copy(rr_vals)
    drop_steps[~drops] = 0
    drop_totals = np.cumsum(drop_steps)
    return ak.Array(rr_vals + drop_totals - drop_steps)

class FileData:
    def __init__(self,t):
        #Turn file fname into data produced by datafunc
        #t=uproot.open(fname)['registerread'].arrays()

        self.cap           = t['cap']
        self.timestamps_events    = t['timestamp']
        self.requested_reg = t['reg']
        self.modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typ, self.evtmask = get_packets_from_cap(self.cap)

        timestamps_packets = get_broadcasted_flattened(self.timestamps_events[self.evtmask == 0], typ)
        modules    = get_broadcasted_flattened(self.modules   [self.evtmask == 0], typ)
        typ = ak.flatten(typ)
        packets = ak.flatten(packets)

        runlengths = ak.run_lengths(timestamps_packets)
        runlengths_all_evts = ak.run_lengths(self.timestamps_events)
        self.packets    = ak.unflatten(packets,   runlengths)
        self.timestamps = ak.unflatten(timestamps_packets,runlengths)
        self.typ        = ak.unflatten(typ,       runlengths)

        p_hccrr=self.packets[self.typ==itsdaq.HCCTYP_HCC_RR]
        self.hccrr = itsdaq.hccrr(p_hccrr)
        p_abcrr=self.packets[self.typ==itsdaq.HCCTYP_ABC_RR]
        self.abcrr = itsdaq.abcrr(p_abcrr)

def corrected_seus(fd, isABC, regno, bitmask, fast_reset = False):
    if isABC:
        regs = fd.abcrr
        rr_typ = itsdaq.HCCTYP_ABC_RR
    else:
        regs = fd.hccrr
        rr_typ = itsdaq.HCCTYP_HCC_RR

    rr_timestamps = ak.flatten(fd.timestamps[fd.typ==rr_typ][regs['address']==regno])
    seu_rrs = itsdaq.get_rr_vals_by_address(regs,regno)

    bits = ak.flatten(count_bits(seu_rrs & bitmask))
    if len(bits) == 0:
        return ak.Array([]), ak.Array([])
    if fast_reset:
        delta = bits[1:]
    else:
        delta = bits[1:]-bits[0:-1]
        delta = ak.to_numpy((delta+abs(delta))/2)
    cumulative = np.cumsum(delta) + bits[0]
    return ak.Array(cumulative), rr_timestamps[1:]

def uncorrected_seus(fd, isABC, blacklist):
    if isABC:
        regs = fd.abcrr
        ref_reg = 63
        rr_typ = itsdaq.HCCTYP_ABC_RR
    else:
        regs = fd.hccrr
        ref_reg = 15
        rr_typ = itsdaq.HCCTYP_HCC_RR

    uncorrected_seu = ak.zeros_like(itsdaq.get_rr_vals_by_address(regs,ref_reg, keep_nones = False))
    for reg_address in regs['address'][0]:
        if reg_address in blacklist:
            continue
        reg_values  = itsdaq.get_rr_vals_by_address(regs, reg_address, keep_nones = False)
        reg_seu_counts = ak.fill_none(count_SEUs(reg_values), 0)
        uncorrected_seu = uncorrected_seu + reg_seu_counts
        
    return uncorrected_seu, ak.flatten(fd.timestamps[fd.typ==rr_typ][regs['address']==ref_reg])

def counter_raw(fd, isABC, regno, bitmask, start):
    if isABC:
        regs = fd.abcrr
        rr_typ = itsdaq.HCCTYP_ABC_RR
    else:
        regs = fd.hccrr
        rr_typ = itsdaq.HCCTYP_HCC_RR

    rr_timestamps = ak.flatten(fd.timestamps[fd.typ==rr_typ][regs['address']==regno])
    rr_vals = itsdaq.get_rr_vals_by_address(regs,regno)
    if len(rr_vals) == 0:
        return ak.Array([]), ak.Array([])
    return ak.flatten( (rr_vals & bitmask) >> start), rr_timestamps

def batch_combine(file_list, fn_dict):
    res_dict = {fname: ([],[]) for fname in fn_dict}
    for batch in uproot.iterate(file_list):
        fd = FileData(batch)
        for (fname, (fn, args)) in fn_dict.items():
            xi, yi = fn(fd, *args)
            res_dict[fname][0].append(xi)
            res_dict[fname][1].append(yi)
    return { k: (ak.flatten(ak.Array(v[0])), ak.flatten(ak.Array(v[1]))) for (k, v) in res_dict.items()}

def batch_combine_cumulative(file_list, fn_dict):
    res_dict = {fname: ([],[]) for fname in fn_dict}
    off_dict = {fname: 0       for fname in fn_dict}
    for batch in uproot.iterate(file_list):
        fd = FileData(batch)
        for (fname, (fn, args, cumulative)) in fn_dict.items():
            xi, yi = fn(fd, *args)
            res_dict[fname][0].append(xi+off_dict[fname])
            res_dict[fname][1].append(yi)
            if cumulative and len(xi) != 0:
                off_dict[fname] += xi[-1]
    return { k: ( ak.flatten(ak.Array(v[0])), ak.flatten(ak.Array(v[1]))-v[1][0][0], ak.count(ak.Array(v[0]),axis=1) ) for (k, v) in res_dict.items()}

def packet_errors(fd):
    bad_evt_mask = fd.evtmask != 0
    return ak.flatten( bad_evt_mask[bad_evt_mask] ), ak.flatten(fd.timestamps_events[bad_evt_mask])

#Setup Input Args
parser = argparse.ArgumentParser()
parser.add_argument('infile_list',type=str)
parser.add_argument('tag',type=str)
parser.add_argument('--uncorr',action='store_true')
parser.add_argument('--corr',action='store_true')
parser.add_argument('--pack',action='store_true')
parser.add_argument('--count',action='store_true')
parser.add_argument('--phys',action='store_true')
parser.add_argument('--bits',action='store_true')
parser.add_argument('--splice',action='store_true')

args = parser.parse_args()
infile = args.infile_list
tag = args.tag
do_uncorr = args.uncorr
do_corr = args.corr
do_pack = args.pack
do_count = args.count
do_phys = args.phys
do_bits = args.bits
do_splice = args.splice

#Parse list of root files to read and combine
file_list = []
with open(infile) as f:
    file_list = f.read().splitlines()

file_dict = {f: "registerread" for f in file_list}

hcc_blacklist = [0,1,4,7,8,9,10,15] #SEU, RO, HPR
abc_blacklist = [48,49,50,51,52,63] + [i for i in range(0x80,0xBF+1)] #SEU1, SEU2, HPR
fast_reset = ('kr' in tag)

#Dict of {fn_label: (fn, args, cumulative)}
fndict = {}
fullmask = 0xffffffff

#Fill function dictionary
if do_uncorr:
    fndict['uncorr_hcc'] = (uncorrected_seus, (False,hcc_blacklist), True)
    fndict['uncorr_abc'] = (uncorrected_seus, (True, abc_blacklist), True)

if do_corr:
    fndict['corr_reg_abc'] = (corrected_seus, (True, 0x30, 0x03ffffff, fast_reset), True)
    fndict['corr_sp1_abc'] = (corrected_seus, (True, 0x30, 0xf0000000, fast_reset), True)
    fndict['corr_sp2_abc'] = (corrected_seus, (True, 0x31, 0x00000700, fast_reset), True)
    fndict['corr_reg_hcc'] = (corrected_seus, (False,   0, 0x0003ffff, fast_reset), True)
    fndict['corr_dec_hcc'] = (corrected_seus, (False,   0, 0x00fc0000, fast_reset), True)
    fndict['corr_hpr_hcc'] = (corrected_seus, (False,   0, 0xc0000000, fast_reset), True)

if do_phys:
    fndict['corr_lpc_hcc'] = (corrected_seus, (False,   1, 0x0000ffff), True)
    fndict['corr_lpu_hcc'] = (corrected_seus, (False,   1, 0xffff0000), True)
    fndict['corr_prc_hcc'] = (corrected_seus, (False,   2, 0x0000ffff), True)
    fndict['corr_pru_hcc'] = (corrected_seus, (False,   2, 0xffff0000), True)

if do_count:
    fndict['cnt_seu_abc']  = (counter_raw,    (True, 0x34, 0x0ff00000, 20), False)
    fndict['cnt_lcbe_abc'] = (counter_raw,    (True, 0x34, 0x000000ff,  0), False)
    fndict['cnt_scme_abc'] = (counter_raw,    (True, 0x34, 0x0000ff00,  8), False)
    fndict['cnt_lcbe_hcc'] = (counter_raw,    (False,   4, 0xffff0000, 16), False)
    fndict['cnt_r3le_hcc'] = (counter_raw,    (False,   4, 0x0000ffff,  0), False)

if do_pack:
    pass

if do_bits:
    fndict['ABC_HPR_Bits'] = (counter_raw, (True, 0x3f, fullmask, 0), False)
    fndict['HCC_HPR_Bits'] = (counter_raw, (False,  15, fullmask, 0), False)

resdir = batch_combine_cumulative(file_dict, fndict)

#Make the Plots
if do_uncorr:
    uncorrected_seus_hcc, t_ush = resdir['uncorr_hcc']
    uncorrected_seus_abc, t_usa = resdir['uncorr_abc']

    make_plots(resdir, ['uncorr_hcc','uncorr_abc'], file_list, 'Plots/Uncorr/'+tag+'_uncorrected_SEU', do_splice, erase_offset = True)
    print('Total of {} HCC and {} ABC uncorrected bitflips'.format(uncorrected_seus_hcc[-1],uncorrected_seus_abc[-1]))

if do_corr:
    seu_count = ['corr_reg_abc','corr_sp1_abc','corr_sp2_abc','corr_reg_hcc','corr_dec_hcc','corr_hpr_hcc']
    regs_list = ['corr_reg_abc','corr_reg_hcc']
    others_list = ['corr_sp1_abc','corr_sp2_abc','corr_dec_hcc','corr_hpr_hcc']

    make_plots(resdir, seu_count,   file_list, 'Plots/Corr/'+tag+'_corrected_SEU', do_splice, erase_offset = True)
    make_plots(resdir, regs_list,   file_list, 'Plots/Corr/'+tag+'_reg_SEU',       do_splice, erase_offset = True)
    make_plots(resdir, others_list, file_list, 'Plots/Corr/'+tag+'_other_SEU',     do_splice, erase_offset = True)

    csvline = tag + ','
    for scount in seu_count:
        counts, times, _ = resdir[scount]
        csvline = csvline + str(counts[-1]) + ','

    print('Corrected SEUs {}'.format(tag))
    print('Run,'+','.join(seu_count))
    print(csvline)
    print()

if do_phys:
    seu_count = ['corr_lpc_hcc','corr_lpu_hcc','corr_prc_hcc','corr_pru_hcc']
    make_plots(resdir, seu_count, file_list, 'Plots/Phys/'+tag+'_phys_pack_SEU', do_splice)
    csvline = tag + ','
    for scount in seu_count:
        counts, times, _ = resdir[scount]
        csvline = csvline + str(counts[-1]) + ','
    print('Phys SEUs {}'.format(tag))
    print('Run,'+','.join(seu_count))
    print(csvline)
    print()

if do_count:
    namelist = ['cnt_seu_abc','cnt_lcbe_abc','cnt_scme_abc','cnt_lcbe_hcc','cnt_r3le_hcc']
    smalllist = ['cnt_lcbe_abc','cnt_lcbe_hcc','cnt_r3le_hcc']
    make_plots(resdir, namelist,  file_list, 'Plots/Count/'+tag+'_counters', do_splice, cumulative = True)
    make_plots(resdir, smalllist, file_list, 'Plots/Count/'+tag+'_errors',   do_splice, cumulative = True)

    csvline = tag + ','
    plt.figure()
    for name in namelist:
        counts, times, _ = resdir[name]
        counts = counter_cumulative(counts)
        csvline = csvline + str(counts[-1]) + ','

    print('Counters {}'.format(tag))
    print('Run,'+','.join(namelist))
    print(csvline)
    print()

if do_pack:
    pass

if do_bits:
    namelist = ['ABC_HPR_Bits', 'HCC_HPR_Bits']
    for name in namelist:
        make_plots(resdir,[name,],file_list,'Plots/Bits/'+tag+'_'+name+'_bits', do_splice, bits = True)
