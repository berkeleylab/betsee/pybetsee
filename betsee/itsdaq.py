import awkward as ak
from . import bitstream as bs

"""
Functions to parse the contents of decoded data based on specifications of the
ITk strips commands.

The decoded datastream is an array of 8-bit words that follows the ordering
of a `bytestream`. In addition to the data words, it can have corresponding
control (`k`) bits.

All data is assumed to be stored in an awkward array with the first axis being
the event.
"""

# Control words
IDLE=0xBC
SOP =0x3c
EOP =0xdc

# HCC Registers
HCCREG_HPR=15

# HCC TYP definitions
HCCTYP_LP        =2
HCCTYP_ABC_RR    =4
HCCTYP_ABC_TRANSP=7
HCCTYP_HCC_RR    =8
HCCTYP_ABC_HPR   =13
HCCTYP_HCC_HPR   =14

ABCTYP_RR=4

HCC_REG_NAMES = dict([ (0, "SEU1"), (1, "SEU2"), (2, "SEU3"), (3, "FrameRaw"), 
    (4,"LCBerr"), (5,"ADCStatus"), (6,"Status"), (15,"HPR"), (17,"Addressing"), 
    (32,"Delay1"), (33,"Delay2"), (34,"Delay3"), (35,"PLL1"), (36,"PLL2"), 
    (37,"PLL3"), (38,"DRV1"), (39,"DRV2"), (40,"ICenable"), (41,"OPmodeLo"), 
    (42,"OPmodeHi"), (43,"Cfg1"), (44,"Cfg2"), (45,"ExtRst"), (46,"ExtRstC"), 
    (47,"ErrCfg"),(48,"ADCcfg") ])

ABCv0_REG_NAMES = dict([(1, "DCS1"), (2, "DCS2"), (3, "DCS3"), (4, "DCS4"), (6, "DCS6"), (7, "DCS7"),
    (32, "Config0"), (33, "Config1"), (34, "Config2"), (35, "Config3"), (36, "Config4"), (37, "Config5"), (38, "Config6"),
    (48, "SEUStat"), (49, "SEUStat2"), (50, "FuseStat"), (51, "ADCStat"), (52,"LCBErr"),
    (63, "HPR"),
    (16, "Mask0"), (17, "Mask1"), (18, "Mask2"), (19, "Mask3"), (20, "Mask4"), (21, "Mask5"), (22, "Mask6"), (23, "Mask7"),
    (64, "TrimLo0" ), (65, "TrimLo1" ), (66, "TrimLo2" ), (67, "TrimLo3" ), (68, "TrimLo4" ), (69, "TrimLo5" ), (70, "TrimLo6" ), (71, "TrimLo7" ),
    (72, "TrimLo8" ), (73, "TrimLo9" ), (74, "TrimLo10"), (75, "TrimLo11"), (76, "TrimLo12"), (77, "TrimLo13"), (78, "TrimLo14"), (79, "TrimLo15"),
    (80, "TrimLo16"), (81, "TrimLo17"), (82, "TrimLo18"), (83, "TrimLo19"), (84, "TrimLo20"), (85, "TrimLo21"), (86, "TrimLo22"), (87, "TrimLo23"),
    (88, "TrimLo24"), (89, "TrimLo25"), (90, "TrimLo26"), (91, "TrimLo27"), (92, "TrimLo28"), (93, "TrimLo29"), (94, "TrimLo30"), (95, "TrimLo31"),
    (96, "TrimHi0"), (97, "TrimHi1"), (98, "TrimHi2"), (99, "TrimHi3"), (100, "TrimHi4"), (101, "TrimHi5"), (102, "TrimHi6"), (103, "TrimHi7"),
    (104, "CalMask0"), (105, "CalMask1"), (106, "CalMask2"), (107, "CalMask3"), (108, "CalMask4"), (109, "CalMask5"), (110, "CalMask6"), (111, "CalMask7"), 
    (128, "Counter0" ), (129, "Counter1" ), (130, "Counter2" ), (131, "Counter3" ), (132, "Counter4" ), (133, "Counter5" ), (134, "Counter6" ), (135, "Counter7" ),
    (136, "Counter8" ), (137, "Counter9" ), (138, "Counter10"), (139, "Counter11"), (140, "Counter12"), (141, "Counter13"), (142, "Counter14"), (143, "Counter15"),
    (144, "Counter16"), (145, "Counter17"), (146, "Counter18"), (147, "Counter19"), (148, "Counter20"), (149, "Counter21"), (150, "Counter22"), (151, "Counter23"),
    (152, "Counter24"), (153, "Counter25"), (154, "Counter26"), (155, "Counter27"), (156, "Counter28"), (157, "Counter29"), (158, "Counter30"), (159, "Counter31"),
    (160, "Counter32"), (161, "Counter33"), (162, "Counter34"), (163, "Counter35"), (164, "Counter36"), (165, "Counter37"), (166, "Counter38"), (167, "Counter39"),
    (168, "Counter40"), (169, "Counter41"), (170, "Counter42"), (171, "Counter43"), (172, "Counter44"), (173, "Counter45"), (174, "Counter46"), (175, "Counter47"),
    (176, "Counter48"), (177, "Counter49"), (178, "Counter50"), (179, "Counter51"), (180, "Counter52"), (181, "Counter53"), (182, "Counter54"), (183, "Counter55"),
    (184, "Counter56"), (185, "Counter57"), (186, "Counter58"), (187, "Counter59"), (188, "Counter60"), (189, "Counter61"), (190, "Counter62"), (191, "Counter63")
])

ABCv1_REG_NAMES = dict([(0, "MAGIC"), (1, "DCS1"), (2, "DCS2"), (3, "DCS3"),
    (32, "Config0"), (33, "Config1"),
    (48, "SEU_STAT0"), (49, "STAT1"), (50, "FuseSTAT2"), (51, "ADC_STAT3"), (52,"STAT4"), (53,"STAT5"), (54,"STAT6"),
    (63, "HPR"),
    (16, "Mask0"), (17, "Mask1"), (18, "Mask2"), (19, "Mask3"), (20, "Mask4"), (21, "Mask5"), (22, "Mask6"), (23, "Mask7"),
    (64, "TrimLo0" ), (65, "TrimLo1" ), (66, "TrimLo2" ), (67, "TrimLo3" ), (68, "TrimLo4" ), (69, "TrimLo5" ), (70, "TrimLo6" ), (71, "TrimLo7" ),
    (72, "TrimLo8" ), (73, "TrimLo9" ), (74, "TrimLo10"), (75, "TrimLo11"), (76, "TrimLo12"), (77, "TrimLo13"), (78, "TrimLo14"), (79, "TrimLo15"),
    (80, "TrimLo16"), (81, "TrimLo17"), (82, "TrimLo18"), (83, "TrimLo19"), (84, "TrimLo20"), (85, "TrimLo21"), (86, "TrimLo22"), (87, "TrimLo23"),
    (88, "TrimLo24"), (89, "TrimLo25"), (90, "TrimLo26"), (91, "TrimLo27"), (92, "TrimLo28"), (93, "TrimLo29"), (94, "TrimLo30"), (95, "TrimLo31"),
    (96, "TrimHi0"), (97, "TrimHi1"), (98, "TrimHi2"), (99, "TrimHi3"), (100, "TrimHi4"), (101, "TrimHi5"), (102, "TrimHi6"), (103, "TrimHi7"),
    (104, "CalMask0"), (105, "CalMask1"), (106, "CalMask2"), (107, "CalMask3"), (108, "CalMask4"), (109, "CalMask5"), (110, "CalMask6"), (111, "CalMask7"), 
    (128, "Counter0" ), (129, "Counter1" ), (130, "Counter2" ), (131, "Counter3" ), (132, "Counter4" ), (133, "Counter5" ), (134, "Counter6" ), (135, "Counter7" ),
    (136, "Counter8" ), (137, "Counter9" ), (138, "Counter10"), (139, "Counter11"), (140, "Counter12"), (141, "Counter13"), (142, "Counter14"), (143, "Counter15"),
    (144, "Counter16"), (145, "Counter17"), (146, "Counter18"), (147, "Counter19"), (148, "Counter20"), (149, "Counter21"), (150, "Counter22"), (151, "Counter23"),
    (152, "Counter24"), (153, "Counter25"), (154, "Counter26"), (155, "Counter27"), (156, "Counter28"), (157, "Counter29"), (158, "Counter30"), (159, "Counter31"),
    (160, "Counter32"), (161, "Counter33"), (162, "Counter34"), (163, "Counter35"), (164, "Counter36"), (165, "Counter37"), (166, "Counter38"), (167, "Counter39"),
    (168, "Counter40"), (169, "Counter41"), (170, "Counter42"), (171, "Counter43"), (172, "Counter44"), (173, "Counter45"), (174, "Counter46"), (175, "Counter47"),
    (176, "Counter48"), (177, "Counter49"), (178, "Counter50"), (179, "Counter51"), (180, "Counter52"), (181, "Counter53"), (182, "Counter54"), (183, "Counter55"),
    (184, "Counter56"), (185, "Counter57"), (186, "Counter58"), (187, "Counter59"), (188, "Counter60"), (189, "Counter61"), (190, "Counter62"), (191, "Counter63")
])

ABC_REG_NAMES = ABCv1_REG_NAMES

def remove_idles(data,k):
    """
    Return `data`, `k` with idle words removed.

    Input shape: (event, bytes)
    Output shape: (event, bytes)
    """
    datamask=word_idle(data,k)
    data=data[~datamask]
    k   =k   [~datamask]
    return data,k

def find_packets(data,k):
    """
    Searches the input datastream for the SOP and EOP bytes. The input
    datastream is then split into substreams with the in-between data.

    This assumes that IDLE words have been removed.
    Second return is the mask to apply to get good events.

    Input shape: (event, bytes)
    Output shape: (event, packet, bytes), (event)
    """
    # Identify position of SOP and EOP
    i=ak.local_index(data)
    s=i[(data== 60)&(k==1)]
    e=i[(data==220)&(k==1)]

    no_s_mask = ak.num(s) != 0
    no_e_mask = ak.num(e) != 0
    no_se_mask = no_s_mask & no_e_mask

    #First and last non-idle should be SOP and EOP
    start_mask = ak.fill_none(s.mask[no_s_mask][:,0] == 0,False)
    end_mask   = ak.fill_none(e.mask[no_e_mask][:,-1]== ak.num(data.mask[no_e_mask]) -1,False)

    #Equal no. of SOP and EOP
    match_mask = ( ak.num(s) == ak.num(e) )

    fullmask = no_se_mask & start_mask & end_mask & match_mask
    statusmask = ~no_s_mask + 2*~no_e_mask + 4*~start_mask + 8*~end_mask + 16*~match_mask

    #Check for packets without start and end
    if not ak.all(no_s_mask):
        #print('BAD PACKETS')
        pass
    if not ak.all(no_e_mask):
        #print('BAD PACKETS')
        pass

    # Split into packets by unflattening using the length of each packet
    c=e[fullmask]-s[fullmask]+1
    #Could supply ak.num(data) as count argument - would keep bad packets and merge HPRs
    mydata=ak.unflatten(data[fullmask],ak.flatten(c),axis=-1)
    myk   =ak.unflatten(k[fullmask]   ,ak.flatten(c),axis=-1)
    # Remove the SOP and EPS's
    mask=~(word_sop(mydata,myk)|word_eop(mydata,myk))
    a=mydata[mask]
    return a, statusmask

def word_sop(data,k):
    """
    Return mask identifying the SOP word.
    """
    return (data== SOP)&(k==1)

def word_eop(data,k):
    """
    Return mask identifying the EOP word.
    """
    return (data== EOP)&(k==1)

def word_idle(data,k):
    """
    Return mask identifying the IDLE word.
    """
    return (data==IDLE)&(k==1)

def packet_typ(packets):
    """
    Extract TYP from the packet data

    Input shape: (event, packet, bytes)
    Output shape: (event, packet, typ)

    Returns:
     int: TYP of packet
    """
    typ=bs.bits(packets,0,4)
    return typ

def get_rr_vals_by_address(rrs, address, keep_nones = False):
    if keep_nones:
        return rrs.mask[rrs['address'] == address]['value']
    else:
        return rrs[ak.fill_none(rrs['address'] == address,False)]['value']

def hccrr(packets):
    enough_words = ak.num(packets,axis=-1) >= 6
    return ak.Array({
        'typ'    : packet_typ   (packets.mask[enough_words]),
        'address': hccrr_address(packets.mask[enough_words]),
        'value'  : hccrr_value  (packets.mask[enough_words])
    })

def hccrr_address(packets):
    return bs.bits(packets, 4, 8)

def hccrr_value(packets):
    return bs.bits(packets,12,32)

def abcrr(packets):
    enough_words = ak.num(packets,axis=-1) >= 9
    return ak.Array({
        'typ'    : packet_typ   (packets.mask[enough_words]),
        'abcno'  : abcrr_abcno  (packets.mask[enough_words]),
        'address': abcrr_address(packets.mask[enough_words]),
        'value'  : abcrr_value  (packets.mask[enough_words]),
        'status' : abcrr_status (packets.mask[enough_words])
    })
def abcrr_abcno(packets):
    return bs.bits(packets,4,4)

def abcrr_address(packets):
    return bs.bits(packets,8,8)

def abcrr_value(packets):
    return bs.bits(packets,20,32)

def abcrr_status(packets):
    return bs.bits(packets,52,16)
