"""
Functions that treat the capture stream (array of basic type, ie: uint32) as a
bitstream.
"""

import numpy as np
import awkward as ak

_dtype_nbits={'uint8':8,'uint32':32,'uint64':64}

def entry_size(captype):
    """
    Return the number of bits used to represent an entry in the capture stream
    array.
    """
    if hasattr(captype,'type'): # Recrusve to get to the last axis
        return entry_size(captype.type) 
    return _dtype_nbits[captype.dtype]

def bit(cap, idx):
    """
    Return the bit at position `idx` in capstream `cap`.
    """
    tnbits=entry_size(cap)

    capidx=idx//tnbits
    bitpos=idx%tnbits
    return (cap[:,capidx]>>bitpos)&1

def left_shift(cap, shift):
    """
    Left shift a capstream by `shift`. The result has length one less.
    """
    tnbits=entry_size(cap)

    l=cap[:, :-1]<<(       shift)
    r=cap[:,1:  ]>>(tnbits-shift)
    return l|r

def bits(cap, start, nbits):
    """
    Return `nbits` bits starting from bit `start`.

    `nbits` must be less than 32 to ensure that the output can be stored inside
    a 32-bit integer.
    """
    # Calculate final mask at the start as nbits will be updated as words are
    # shifted to the result.
    finalmask=(2**nbits-1)

    # Find the start and end words
    tnbits=entry_size(cap)
    istart=start//tnbits
    iend=(start+nbits-1)//tnbits

    # First bit position in the start word
    result=None
    for i in range(istart,iend+1):
        # Bit positions in the current word
        bstart=start%tnbits
        blen  =min(nbits,tnbits-bstart)
        shift =tnbits-bstart-blen # bits to shift current word to make LSB valid

        if result is None: # initialize by copy and turn to uint32
            result=ak.values_astype(cap[...,i], np.uint32)
            result=result>>shift # Shift for LSB to be last valid bit
        else:
            # Make space for new addition and add it
            result=(result<<blen) | (cap[...,i]>>shift)

        # Update remaining bits to fetch
        start+=blen
        nbits-=blen

    # Get the bits we want
    result=result&finalmask
    return result
