mkdir Plots/Logs
for file_list in $(ls file_lists/rreads_OUT_*txt); do 
#for file_list in $(ls file_lists/rreads_OUT_*txt | grep -E 'cr|kr|al'); do 
    tag=$(echo $file_list | sed 's:file_lists/rreads_::;s:.txt::')
    echo $file_list
    python3 betsee_analysis.py $file_list $tag --corr --phys --count --bits --splice > Plots/Logs/${tag}.txt
done
