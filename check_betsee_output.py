# %% Import useful packages
import uproot
import awkward as ak
import numpy as np

import argparse
import sys

import betsee.itsdaq as itsdaq
import betsee.bitstream as bs
import betsee.enc8b10b as enc8b10b

# %% Prepare configuration
infile = None
if 'ipykernel' in sys.modules: # running in a notebook
    # %load_ext autoreload
    # %autoreload 2
    infile = '/home/kkrizka/mnt/beefairy/setup_March2022_SEE/data/BETSEE/data/strun111_0.root'
else:
    parser = argparse.ArgumentParser('check_betsee_output')
    parser.add_argument('infile',help='File containing output stream from monitor.C. Looks like strun###_#.root')
    parser.add_argument('--parsing_errors', '-e', help='Print Parsing Errors', action='store_true')
    parser.add_argument('--hprs', '-p', help='Print Unexpected HPRs', action='store_true')
    parser.add_argument('--HCC_statuses', '-s', help='Dump HCC statuses', action='store_true')
    args = parser.parse_args()
    infile = args.infile
    parse_packets = args.hprs or args.HCC_statuses

# %% Open input file
t=uproot.open(infile)['registerread'].arrays()

# %% Metadata on expetected values
#print(t.fields)
t['isHCC'] = t['reg'] >= 256
t['addr' ] = t['reg'] % 256

# %% Parse the capture stream to extract packets
cap = t['cap']
acap      =enc8b10b.align (cap)
data,k    =enc8b10b.decode(acap)
mydata,myk=itsdaq.remove_idles(data,k)
packets,statusmask   =itsdaq.find_packets(mydata,myk)
typ       =itsdaq.packet_typ  (packets)
typmask = (typ != 4) & (typ != 8)
typmask_evt = ak.sum(typmask, axis=-1) != 0

if ak.count_nonzero(statusmask) or ak.count_nonzero(typmask_evt):
    print(infile)

if args.parsing_errors:
    for index in ak.local_index(statusmask)[statusmask != 0]:
        print('Parsing error 0b{:06b} in Event {} Timestamp {}'.format(statusmask[index],index,t['timestamp'][index]))
        datastream = ''
        has_zeros = False
        for d in data[index]:
            if d == 0:
                has_zeros = True
                #continue
            datastream+= '0x{:02X} '.format(d)
        acapstream = ''
        if datastream != '' and has_zeros:
            all_zeros = True
            for word in acap[index]:
                if word != 0:
                    all_zeros = False
                acapstream += '0x{:08X} '.format(word)
            if all_zeros:
                print('Packet of 0s')
            else:
                print('Misalignment')
                print(acapstream)
        else:
            print(datastream)

if parse_packets:
    p_rr = (typ==itsdaq.HCCTYP_HCC_RR) | (typ==itsdaq.HCCTYP_ABC_RR)

    abc_rr = itsdaq.abcrr(packets[typ==itsdaq.HCCTYP_ABC_RR])
    hcc_rr = itsdaq.hccrr(packets[typ==itsdaq.HCCTYP_HCC_RR])

    abc_hpr = itsdaq.abcrr(packets[typ==itsdaq.HCCTYP_ABC_HPR])
    hcc_hpr = itsdaq.hccrr(packets[typ==itsdaq.HCCTYP_HCC_HPR])

if args.hprs:
    pkt_indices = ak.local_index(typ, axis=0)[typmask_evt]
    evt_indices = ak.local_index(t['reg'], axis=0)[statusmask == 0][typmask_evt]
    for pkt, evt in zip(pkt_indices, evt_indices):
        print('Packet {} Event {} Timestamp {}'.format(pkt,evt,t['timestamp'][evt]))
        print('Unexepcted event with typs ',typ[pkt])
        datastream = ''
        for d in mydata[evt]:
            datastream+= '0x{:02X} '.format(d)
        print(datastream)

        if itsdaq.HCCTYP_ABC_HPR in typ[pkt]:
            print('ABC HPRs')
            abc_hpr = itsdaq.abcrr(packets[pkt][typ[pkt]==itsdaq.HCCTYP_ABC_HPR])
            outs = ''
            for v in abc_hpr['value']:
                print('0x{:08X}'.format(v))

        if itsdaq.HCCTYP_HCC_HPR in typ[pkt]:
            print('HCC HPRs')
            hcc_hpr = itsdaq.hccrr(packets[pkt][typ[pkt]==itsdaq.HCCTYP_HCC_HPR])
            outs = ''
            for v in hcc_hpr['value']:
                print('0x{:08X}'.format(v))

if args.HCC_statuses:
    regs = [0,1,2,3,4,5,6,15]
    vals = [ak.flatten(hcc_rr[hcc_rr['address'] == addr]['value']) for addr in regs]

    print(('  {:08d} '*len(regs)).format(*regs))
    for val in zip(*vals):
        print(('0x{:08X} '*len(val)).format(*val))

#fields=['address','value', 'typ']
#p_rr=ak.concatenate([abc_rr[fields], hcc_rr[fields]], axis=1)
# for field in fields:
#     # The firsts is the default for when there are multiple packets
#     t[field] = ak.firsts(p_rr[field])
#     pass

# # %% Print information about the first packet
# mypackets=t[0:1]
# if typ[0] == itsdaq.HCCTYP_HCC_RR:
#     print('First RR is HCC')
#     print('Address: ',mypackets['address'][0])
#     print('Value: 0x{:08X}'.format(mypackets['value'][0]))
# else:
#     print('First RR is ABC')
#     print('Address: ',mypackets['address'][0])
#     print('Value: 0x{:08X}'.format(mypackets['value'][0]))

# # %% Check for no response
# numpackets=ak.num(packets)
# pempty=np.count_nonzero((numpackets==0) & ( t.isHCC))
# if pempty>0:
#     print(f'Number of empty HCC responses: {pempty}')

# # %% Check for no response
# numpackets=ak.num(packets)
# pempty=np.count_nonzero((numpackets==0) & (~t.isHCC))
# if pempty>0:
#     print(f'Number of empty ABC responses: {pempty}')

# # %% Check for too many responses
# numpackets=ak.num(packets)
# ptoomany=np.count_nonzero(numpackets>1)
# if ptoomany>0:
#     print(f'Too many responses responses: {ptoomany}')

# # %% Check for wrong address
# pwrong=np.count_nonzero(t['addr']!=t['address'])
# if pwrong>0:
#     print(f'Wrong address: {pwrong}')

# # %% Check for wrong TYP
# pwrhcc=np.count_nonzero((t['typ']==itsdaq.HCCTYP_HCC_RR) & (t['isHCC']==False))
# if pwrhcc>0:
#     print(f'Expected ABC, got HCC: {pwrhcc}')

# # %% Check for wrong TYP
# pwrhcc=np.count_nonzero((t['typ']==itsdaq.HCCTYP_ABC_RR) & (t['isHCC']==True ))
# if pwrhcc>0:
#     print(f'Expected HCC, got ABC: {pwrhcc}')

# %% Example of how to debug buggy events
# # Check first ABC response
# my     =t   [~t.isHCC][0]
# my_cap =cap [~t.isHCC][0]
# my_acap=acap[~t.isHCC][0]
# my_data=data[~t.isHCC][0]

# print('Raw Capture Stream')
# print(['{:08X}'.format(x) for x in my_cap])
# print('Aligned Capture Stream')
# print(['{:08X}'.format(x) for x in my_acap])
# print('Decoded Capture Stream')
# print(['{:02X}'.format(x) for x in my_data])
