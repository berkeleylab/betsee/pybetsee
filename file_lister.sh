maindir=$(realpath ~/../kkrizka/UCL_March2022_SEE/)
datapath=$(realpath $maindir/data/BETSEE/data/)/

for outdir in $(ls $maindir | grep '^OUT'); do  
    echo $outdir
    grep "dumped" ${maindir}/${outdir}/itsdaq.out \
        | cut -d ' ' -f 4 \
        | sed s:.*/:$datapath: \
        > file_lists/rreads_${outdir}.txt
done

for outdir in $(ls ${maindir} | grep '^OUT'); do  
    echo $outdir
    grep "run_nmask" ${maindir}/${outdir}/itsdaq.out \
        | cut -d ' ' -f 3,5 \
        | xargs printf "${maindir}/data/BETSEE/data/strun%s_%s.root\n" \
        | grep -v strun_ \
        | sed s:.*/:$datapath: \
        > file_lists/scans_${outdir}.txt
done

find file_lists -type f -empty -delete
