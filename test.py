# %%
%load_ext autoreload
%autoreload 2

# %%
import uproot
import awkward as ak
import numpy as np
import betsee.itsdaq as itsdaq
import betsee.bitstream as bs
import betsee.enc8b10b as enc8b10b

# %% Load input datas
#f0='strun417_4.root'
#f='strun462_31.root'
fn='strun94_0.root'
t=uproot.open(fn)['registerread'].arrays()

# %% Align bits based on idle word
cap=t['cap']
acap=enc8b10b.align(cap)

nbits=len(acap[0])*32 # Assume all streams have the same length

#%% First word of all captured events
for x in bs.bits(acap,0,10):
    print('0x{:02X}'.format(enc8b10b.dec_8b10b[x]))

# %% All words in the first event
mycap=acap[0:1]
for i in range(nbits//10):
    x=bs.bits(mycap,10*i,10)
    print('{}\t0x{:02X}'.format(i,enc8b10b.dec_8b10b[x[0]]))

# %% Decode capture stream
data,k=enc8b10b.decode(acap)

# %%
mydata,myk=itsdaq.remove_idles(data,k)
print(mydata)
print(myk)

# %% Split packets

# Double up some packets to test the case where you have multiple in a single
# capture stream
tdata=ak.concatenate([mydata,mydata],axis=1)
tk=ak.concatenate([myk,myk],axis=1)
tdata=ak.concatenate([tdata,mydata],axis=0)
tk=ak.concatenate([tk,myk],axis=0)

packets=itsdaq.find_packets(tdata,tk)
packets

# %% Identify packet TYP
typ=itsdaq.packet_typ(packets)
typ[0]

# %% Select HCC RR's
p_hccrr=packets[typ==itsdaq.HCCTYP_HCC_RR]
p_hccrr

# %%
hccrr=itsdaq.hccrr(p_hccrr)
for a,v in zip(ak.flatten(hccrr['address']),ak.flatten(hccrr['value'])):
    print('0x{:02X}\t0x{:08X}'.format(a,v))

# %%
hpr=hccrr[hccrr['address']==itsdaq.HCCREG_HPR]
for a,v in zip(ak.flatten(hpr['address']),ak.flatten(hpr['value'])):
    print('0x{:02X}\t0x{:08X}'.format(a,v))

# %%
