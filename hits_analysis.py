import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import uproot
import awkward as ak
import betsee.itsdaq as itsdaq
import betsee.bitstream as bs
import betsee.enc8b10b as enc8b10b
import argparse
import time
import os
import datetime

S2NS = 1000000000

def prepare_plot(ylabel = '', bits = False):
    if bits:
        plt.ylabel('Time (arb.)')
        plt.xlabel('Channel')
    else:
        plt.xlabel('Elapsed time [s]')
        plt.ylabel(ylabel)
        plt.legend()
        plt.ylim(bottom=0)

def make_plots(datalist, times, prefix, splice, cumulative=False, bits=False):
    if splice:
        os.makedirs(prefix+'_split/', exist_ok = True)
        stride = 50
        index = 0
        while index < len(times):
            start = index
            end = index+stride
            if end > len(times):
                end = len(times)
            plt.figure()
            for data, name in datalist:
                data_slice = data[start:end]
                if bits:
                    plt.matshow(data_slice, aspect = 'auto')
                else:
                    plt.plot(times[start:end], data_slice, label=name)
            prepare_plot(bits=bits)
            fname = 'Files_{}_{}'.format(start,end)
            plt.title(fname)
            plt.savefig(prefix+'_split/'+fname+'.png')
            plt.close()
            index = end
    
    plt.figure()
    for data, name in datalist:
        if bits:
            plt.matshow(data, aspect = 'auto')
        else:
            plt.plot(times, data, label=name)
    prepare_plot(bits=bits)
    plt.savefig(prefix+'.png')

    if cumulative:
        plt.figure()
        for data, name in datalist:
            plt.plot(times, np.cumsum(data), label=name)
        prepare_plot(bits=bits)
        plt.savefig(prefix+'_cumulative.png')

class FileData:
    def __init__(self,rootfile):
        hist0 = rootfile['h_scan0'].values()[:,0]
        hist1 = rootfile['h_scan1'].values()[:,0]
        time_string = rootfile['Time']

        self.time = epoch_time = time.mktime(datetime.datetime.strptime(time_string, "%a %b %d %H:%M:%S %Y").timetuple())
        self.hits = np.concatenate((hist0,hist1))
        hitsbool = self.hits != 0

        ran = np.arange(0,len(self.hits))
        mask = ( (ran % len(hist0)) >= 1280) & (ran % 2 == 0)
        self.total_hits   = np.sum( self.hits )
        self.good_hits    = np.sum( mask &  hitsbool)
        self.bad_hits     = np.sum(~mask &  hitsbool)
        self.missing_hits = np.sum( mask & ~hitsbool)

def collect_hits(file_list):
    times = []
    hits = []
    for fname in file_list:
        t = uproot.open(fname)
        if not 'h_scan0' in t:
            continue
        fd = FileData(t)
        times.append(fd.time)
        hits.append(fd.hits)
    return ak.Array(hits), ak.Array(times)

parser = argparse.ArgumentParser()
parser.add_argument('infile_list',type=str)
parser.add_argument('tag',type=str)
parser.add_argument('--raw',action='store_true')
parser.add_argument('--count',action='store_true')
parser.add_argument('--splice',action='store_true')

args = parser.parse_args()
infile = args.infile_list
tag = args.tag
do_splice = args.splice

#Parse list of root files to read and combine
file_list = []
with open(infile) as f:
    file_list = f.read().splitlines()

hits, times = collect_hits(file_list)
times = (times - times[0])

if args.raw:
    make_plots( ((hits,'Raw_Hits'),),times,'Plots/RawHits/'+tag+'_raw_hits', do_splice, bits=True)

if args.count:
    mask = hits[0]
    hitsnp = hits.to_numpy() == 1
    masknp = mask.to_numpy() == 1
    total_hits   = (ak.sum( hits, axis=1), 'Total_Hits')
    good_hits    = (ak.sum( hitsnp &  masknp, axis=1), 'Good_Hits')
    bad_hits     = (ak.sum( hitsnp & ~masknp, axis=1), 'Bad_Hits')
    missing_hits = (ak.sum(~hitsnp &  masknp, axis=1), 'Missing_Hits')
    full_dlist = [total_hits, good_hits, bad_hits, missing_hits]
    low_list = [bad_hits, missing_hits]
    make_plots(full_dlist, times, 'Plots/Hits/'+tag+'_all_hits', do_splice)
    make_plots(low_list,   times, 'Plots/Hits/'+tag+'_hit_errs', do_splice, cumulative = True)