mkdir Plots/BadPackets
for file_list in $(ls file_lists/rread*txt); do 
#for file_list in $(ls file_lists/rread*txt | grep -E 'cr|kr|al'); do 
    echo $file_list
    rm Plots/BadPackets/${file_list##*/}
    for file in $(cat $file_list); do 
        python3 check_betsee_output.py $file -ep >> Plots/BadPackets/${file_list##*/}
    done
done
