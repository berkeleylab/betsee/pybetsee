import re
from itertools import zip_longest
import numpy as np
import uproot
import awkward as ak
import betsee.itsdaq as itsdaq
import betsee.bitstream as bs
import betsee.enc8b10b as enc8b10b
import time
import datetime

S2NS = 1000000000

def count_bits(arr):
    out = ak.zeros_like(arr)
    for bit in range(32):
        num = 1 << bit
        out = out + ( (arr & num) == num )
    return out

def count_SEUs(seu_reg):
    #Check for at least on SEU RR
    if ak.count(seu_reg) == 0:
        return ak.Array([0])
    #Get first SEU RR
    golden = ak.flatten(seu_reg)[0]
    #Find bits that flipped
    flipped = seu_reg ^ golden
    #Count them
    return ak.firsts(count_bits(flipped))

def get_packets_from_cap(cap, flat = True):
    #Digest the stream into packets of 8b words
    acap=enc8b10b.align(cap)
    data,k=enc8b10b.decode(acap)
    mydata,myk=itsdaq.remove_idles(data,k)
    packets, evtmask = itsdaq.find_packets(mydata,myk)
    #packets=ak.flatten(packets)
    typ=itsdaq.packet_typ(packets)
    return packets, typ, evtmask

#Use case: have an array of e.g. timestamps from good events, need to match them to packets
def get_broadcasted_flattened(array, packet_shape):
    _, array = ak.broadcast_arrays(packet_shape, array)
    return ak.flatten(array)
    
#Eater to pass on data points per packet from a captured data stream
class PacketEater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']
        isHCC         = t['reg'] >= 256

        #Digest the stream into packets of 8b words
        packets, typs, evtmask = get_packets_from_cap(cap)

        timestamps = get_broadcasted_flattened(timestamps[evtmask == 0], typs)
        isHCC      = get_broadcasted_flattened(isHCC     [evtmask == 0], typs)
        modules    = get_broadcasted_flattened(modules   [evtmask == 0], typs)
        typs = ak.flatten(typs)

        points+=[{'time':time*S2NS, 
            'fields':{'typ': typ}, 
            'tags':{'module': module, 'isHCC': isHCC}} for time,typ,isHCC,module in 
            zip(timestamps,typs,isHCC,modules)]

        return points


class HCC_HPREater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typs, evtmask = get_packets_from_cap(cap)

        timestamps = get_broadcasted_flattened(timestamps[evtmask == 0], typs)
        modules    = get_broadcasted_flattened(modules   [evtmask == 0], typs)
        typs = ak.flatten(typs)
        packets = ak.flatten(packets)

        m_hccrr = typs==itsdaq.HCCTYP_HCC_RR
        p_hccrr = packets[m_hccrr]
        hccrr   = itsdaq.hccrr(p_hccrr)
        hpr_mask = ak.fill_none(hccrr['address']==15,False) #REG15 HPR

        hcc_hpr_reg  = hccrr[hpr_mask]['value'] 
        times = timestamps[m_hccrr][hpr_mask]
        modules = modules[m_hccrr][hpr_mask]

        points+=[{'time':time*S2NS, 
            'fields':{
                'Raw_HPR':hpr,
                'HCC_R3L1_lock':        bool(hpr & (1 << 0)),
                'HCC_LCB_lock':         bool(hpr & (1 << 1)), 
                'HCC_LCB_Decode_Error': bool(hpr & (1 << 2)),
                'HCC_LCB_ErrCnt_Ovflw': bool(hpr & (1 << 3)),
                'HCC_LCB_SCMD_Error':   bool(hpr & (1 << 4)),
                'HCC_PLL_Loack_Ind':    bool(hpr & (1 << 5)),
                'R3L1_ErrCnt_Ovflw':    bool(hpr & (1 << 6)),
                'HCC_LCB_frame_raw':(hpr >> 16)
                }, 
            'tags':{'module': module}} for time,hpr,module in 
            zip(times,hcc_hpr_reg,modules)]

        return points

class ABC_HPREater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typs, evtmask = get_packets_from_cap(cap)

        timestamps = get_broadcasted_flattened(timestamps[evtmask == 0], typs)
        modules    = get_broadcasted_flattened(modules   [evtmask == 0], typs)
        typs = ak.flatten(typs)
        packets = ak.flatten(packets)

        m_abcrr = typs==itsdaq.HCCTYP_ABC_RR
        p_abcrr = packets[m_abcrr]
        abcrr   = itsdaq.abcrr(p_abcrr)
        hpr_mask = ak.fill_none(abcrr['address']==63,False) #REG 0x3f HPR

        abc_hpr_reg  = abcrr[hpr_mask]['value'] 
        times = timestamps[m_abcrr][hpr_mask]
        modules = modules[m_abcrr][hpr_mask]

        points+=[{'time':time*S2NS, 
            'fields':{
                'Raw_HPR':hpr,
                'ADC_dat_float':hpr & 0xfff, 
                'ABC_LCB_Locked'     :bool( (hpr >> 12) & 0b1),
                'ABC_LCB_Decode_Err' :bool( (hpr >> 13) & 0b1),
                'ABC_LCB_ErrCnt_Ovfl':bool( (hpr >> 14) & 0b1),
                'ABC_LCB_SCmd_Err'   :bool( (hpr >> 15) & 0b1),
                'ABC_LCB_S':(hpr >> 16)
                }, 
            'tags':{'module': module}} for time,hpr,module in 
            zip(times,abc_hpr_reg,modules)]

        return points

class ABC_StatusEater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typs, evtmask = get_packets_from_cap(cap)

        timestamps = get_broadcasted_flattened(timestamps[evtmask == 0], typs)
        modules    = get_broadcasted_flattened(modules   [evtmask == 0], typs)
        typs = ak.flatten(typs)
        packets = ak.flatten(packets)

        m_abcrr = typs==itsdaq.HCCTYP_ABC_RR
        p_abcrr = packets[m_abcrr]
        abcrr   = itsdaq.abcrr(p_abcrr)

        abc_status  = abcrr['status'] 
        times = timestamps[m_abcrr]
        modules = modules[m_abcrr]

        points+=[{'time':time*S2NS, 
            'fields':{
                'Raw_Status':status,
                'ClusterFIFO_empty'       :bool( ( status >>  0) & 0b1),
                'ClusterFIFO_almost_full' :bool( ( status >>  1) & 0b1),
                'ClusterFIFO_overflow'    :bool( ( status >>  2) & 0b1),
                'RegFIFO_empty'           :bool( ( status >>  3) & 0b1),
                'RegFIFO_almost_full'     :bool( ( status >>  4) & 0b1),
                'RegFIFO_overflow'        :bool( ( status >>  5) & 0b1),
                'LPFIFO_empty'            :bool( ( status >>  6) & 0b1),
                'LPFIFO_almost_full'      :bool( ( status >>  7) & 0b1),
                'PRFIFO_empty'            :bool( ( status >>  8) & 0b1),
                'PRFIFO_almost_full'      :bool( ( status >>  9) & 0b1),
                'BCIDFlag'                :bool( ( status >> 10) & 0b1),
                'SEUFlag'                 :bool( ( status >> 11) & 0b1),
                'chipID'                  : status >> 12
                }, 
            'tags':{'module': module}} for time,status,module in 
            zip(times,abc_status,modules)]

        return points

class BadEventEater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typ, evtmask = get_packets_from_cap(cap)
        
        points+=[{'time':time*S2NS, 
            'fields':{'expected_reg':reg}, 
            'tags':{'module': module,
                'SOP/EOP mismatch': bool(status & 16),
                'bad end':          bool(status &  8), 
                'bad start':        bool(status &  4), 
                'no EOP':           bool(status &  2),
                'no SOP':           bool(status &  1)
            }} 
            for time,status,module,reg in 
            zip(timestamps[evtmask != 0],evtmask[evtmask != 0],modules[evtmask != 0],requested_reg[evtmask != 0])]

        return points

class RegisterDumpEater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')
        #Blacklisted due to scan: HCC 48, ABC 3, ABC 16-24
        self.hcc_blacklist = [0,1,4,5,6,7,8,9,10,15,48] #SEU, RO, HPR
        self.abc_blacklist = [3,48,49,50,51,52,63] + [i for i in range(0x80,0xBF+1)] + [i for i in range(16,24)] #SEU1, SEU2, HPR

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typ, evtmask = get_packets_from_cap(cap)

        timestamps_packets = get_broadcasted_flattened(timestamps[evtmask == 0], typ)
        modules    = get_broadcasted_flattened(modules   [evtmask == 0], typ)
        typ = ak.flatten(typ)
        packets = ak.flatten(packets)

        runlengths = ak.run_lengths(timestamps_packets)
        runlengths_all_evts = ak.run_lengths(timestamps)
        
        packets    = ak.unflatten(packets,   runlengths)
        timestamps = ak.unflatten(timestamps_packets,runlengths)
        typ        = ak.unflatten(typ,       runlengths)
        modules    = ak.unflatten(modules,   runlengths)

        nPackets = ak.num(packets)
        nBadEvents = ak.count_nonzero( ak.unflatten(evtmask,runlengths_all_evts) ,axis=1)
        typlist = [2,4,7,8,13,14]
        packets_of_typ = []
        for t in typlist:
            packets_of_typ.append(ak.count_nonzero(typ == t ,axis=1))

        p_hccrr=packets[typ==itsdaq.HCCTYP_HCC_RR]
        hccrr = itsdaq.hccrr(p_hccrr)
        p_abcrr=packets[typ==itsdaq.HCCTYP_ABC_RR]
        abcrr = itsdaq.abcrr(p_abcrr)   
                    
        #keys will be reg address (HCC addresses are +256)
        #vals will be ak Array of dimension (number of loops, 1)
        #vals contain no. of SEU's detected per loop.
        uncorrected_hcc_seu = ak.zeros_like(ak.flatten(itsdaq.get_rr_vals_by_address(hccrr,0, keep_nones = False)))
        uncorrected_abc_seu = ak.zeros_like(ak.flatten(itsdaq.get_rr_vals_by_address(abcrr,48, keep_nones = False)))
        reg_counter = {}
        track_sum = False
        for reg_address in hccrr['address'][0]:
            if reg_address == None:
                continue
            reg_values  = itsdaq.get_rr_vals_by_address(hccrr, reg_address, keep_nones = False)
            reg_seu_counts = ak.fill_none(count_SEUs(reg_values), 0)
            reg_counter[reg_address+256] = reg_seu_counts
            if not reg_address in self.hcc_blacklist and track_sum:
                if ak.count_nonzero(reg_seu_counts):
                    print(reg_address,' for hcc has seu')
                uncorrected_hcc_seu = uncorrected_hcc_seu + reg_seu_counts

        for reg_address in abcrr['address'][0]:
            if reg_address == None:
                continue
            reg_values  =itsdaq.get_rr_vals_by_address(abcrr, reg_address, keep_nones = False)
            reg_seu_counts = ak.fill_none(count_SEUs(reg_values), 0)
            reg_counter[reg_address] = reg_seu_counts
            if not reg_address in self.abc_blacklist and track_sum:
                if ak.count_nonzero(reg_seu_counts):
                    print(reg_address,' for hcc has seu')
                uncorrected_abc_seu = uncorrected_abc_seu + reg_seu_counts

        hcc_seu_reg  = itsdaq.get_rr_vals_by_address(hccrr, 0)  #REG0: SEU1
        hcc_seu_reg2 = itsdaq.get_rr_vals_by_address(hccrr, 1)  #REG1: SEU2
        hcc_seu_reg3 = itsdaq.get_rr_vals_by_address(hccrr, 2)  #REG1: SEU3
        abc_seu_reg1 = itsdaq.get_rr_vals_by_address(abcrr,48)  #REG 0x30: STAT0 - SEU flags for 32 registers
        abc_seu_reg2 = itsdaq.get_rr_vals_by_address(abcrr,49)  #REG 0x31: STAT1 - 3 non-reg SEU flags
        abc_seu_reg3 = itsdaq.get_rr_vals_by_address(abcrr,52)  #REG 0x34: STAT4 - SEU Counter for 6 regs
        hcc_cnt_reg  = itsdaq.get_rr_vals_by_address(hccrr, 4)

        seu_count_dict = {
            "HCC Corr Reg SEUs":        count_SEUs(hcc_seu_reg  & 0x0001ffff),
            "HCC Corr PRLP SEUs":       count_SEUs(hcc_seu_reg2 & 0x000007ff)+count_SEUs(hcc_seu_reg3 & 0x000007ff),
            "HCC Uncorr PRLP SEUs":     count_SEUs(hcc_seu_reg2 & 0x07ff0000)+count_SEUs(hcc_seu_reg3 & 0x07ff0000),
            "ABC Corr Reg SEUs":        count_SEUs(abc_seu_reg1 & 0x03ffffff),
            "ABC Corr Counted SEUs":    (ak.flatten(abc_seu_reg3) >> 24) & 0xff,
            "ABC Corr Special SEUs":    count_SEUs( (abc_seu_reg1 >> 28) & 0xf ) + count_SEUs( (abc_seu_reg2 >> 8) & 0x7 ),
            "ABC LCB Err Count":        ak.flatten(abc_seu_reg3 & 0xff),
            "ABC LCM SCMD Err Count":   ak.flatten(abc_seu_reg3 >> 8) & 0xff,
            "HCC LCB Err Count":        ak.flatten(hcc_cnt_reg >> 16) & 0xffff,
            "HCC R3L1 Err Count":       ak.flatten(hcc_cnt_reg) & 0xffff
        }

        irange = np.arange(len(timestamps))

        #Hack
        def dict_combine(d1, d2):
            d1.update(d2)
            return d1

        def list_dict_combine(list_of_dicts):
            d1 = list_of_dicts[0]
            for d in list_of_dicts[1:]:
                d1.update(d)
            return d1

        points+=[{'time':timestamps_i[0]*S2NS, 
                    'fields':list_dict_combine( [
                        {'nPackets':nPackets_i, 'nBadEvents':nBadEvents_i, 'hcc_seu_reg': hcc_seu_reg_i, 'abc_seu_reg1': abc_seu_reg1_i, 'abc_seu_reg2': abc_seu_reg2_i, 'Uncorrected ABC SEU':uncabc, 'Uncorrected HCC SEU':unchcc, 'Uncorrected SEU':unchcc+uncabc}, 
                        {'seu_count_reg_'+ str(k) : v[i] for (k, v) in reg_counter.items()}, 
                        {'nPackets_typ_'+str(t) : tcount[i] for (t, tcount) in zip(typlist,packets_of_typ)},
                        {k: v[i] for (k, v) in seu_count_dict.items()}
                    ] ), 
            'tags':{'module': modules_i[0]}} 
            for timestamps_i, nPackets_i, nBadEvents_i, modules_i, hcc_seu_reg_i, abc_seu_reg1_i, abc_seu_reg2_i, i, unchcc, uncabc
                 in zip_longest(timestamps,nPackets,nBadEvents,modules,ak.flatten(hcc_seu_reg),ak.flatten(abc_seu_reg1),ak.flatten(abc_seu_reg2),irange,uncorrected_hcc_seu,uncorrected_abc_seu)]

        return points

class HCC_CounterEater:
    def __init__(self):
        self.re_datasaved=re.compile('Registers dumped to: (.*)')
        #Blacklisted due to scan: HCC 48, ABC 3, ABC 16-24, exclued 11 HCC, 7+64+8
        self.hcc_blacklist = [0,1,4,5,6,7,8,9,10,15,48] #SEU, RO, HPR
        self.abc_blacklist = [3,48,49,50,51,52,63] + [i for i in range(0x80,0xBF+1)] + [i for i in range(16,24)] #SEU1, SEU2, HPR

    def parse_line(self, line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        data=match.group(1)

        t=uproot.open(data)['registerread'].arrays()

        points = []

        cap           = t['cap']
        timestamps    = t['timestamp']
        requested_reg = t['reg']
        modules       = t['module']

        #Digest the stream into packets of 8b words
        packets, typ, evtmask = get_packets_from_cap(cap)

        timestamps_packets = get_broadcasted_flattened(timestamps[evtmask == 0], typ)
        modules    = get_broadcasted_flattened(modules   [evtmask == 0], typ)
        typ = ak.flatten(typ)
        packets = ak.flatten(packets)

        runlengths = ak.run_lengths(timestamps_packets)
        runlengths_all_evts = ak.run_lengths(timestamps)
        
        packets    = ak.unflatten(packets,   runlengths)
        timestamps = ak.unflatten(timestamps_packets,runlengths)
        typ        = ak.unflatten(typ,       runlengths)
        modules    = ak.unflatten(modules,   runlengths)

        p_hccrr=packets[typ==itsdaq.HCCTYP_HCC_RR]
        hccrr = itsdaq.hccrr(p_hccrr)

        hcc_cnt_reg  = itsdaq.get_rr_vals_by_address(hccrr, 4)

        seu_count_dict = {
            "HCC LCB Err Count":        ak.flatten(hcc_cnt_reg >> 16) & 0xffff,
            "HCC R3L1 Err Count":       ak.flatten(hcc_cnt_reg) & 0xffff
        }

        #Hack
        def dict_combine(d1, d2):
            d1.update(d2)
            return d1

        def list_dict_combine(list_of_dicts):
            d1 = list_of_dicts[0]
            for d in list_of_dicts[1:]:
                d1.update(d)
            return d1

        irange = np.arange(len(timestamps))
        points+=[{'time':timestamps_i[0]*S2NS, 
            'fields': {k: v[i] for (k, v) in seu_count_dict.items()}, 
            'tags':{'module': modules_i[0]}} 
            for timestamps_i, modules_i, i
                 in zip_longest(timestamps,modules,irange)]

        return points

class DigitalScanEater:
    def __init__(self):
        self.re_datasaved=re.compile('run_nmask run ([0-9]+) scan ([0-9]+)')

    def parse_line(self,line):
        match=self.re_datasaved.match(line)
        if match==None: # Not new data
            return
        runno=match.group(1)
        scanno=match.group(2)
        scanno=int(scanno)-1
        if scanno == '1':
            return []
        #Gross hard-coding for now
        fname = '../../data/BETSEE/data/strun{}_{}.root'.format(runno,scanno)
        t=uproot.open(fname)

        time_string = t['Time']
        epoch_time = time.mktime(datetime.datetime.strptime(time_string, "%a %b %d %H:%M:%S %Y").timetuple())
        evenarr = t['h_scan0'].to_numpy()[0][:,0]
        oddarr  = t['h_scan1'].to_numpy()[0][:,0]
        evenhits = np.sum(evenarr)
        oddhits = np.sum(oddarr)
        ebool = (evenarr == 1)
        obool = (oddarr == 1)

        ran = np.arange(0, len(evenarr))
        mask = (ran >= 1280) & (ran % 2 == 0)
        good_hits    = np.sum(mask & ebool) + np.sum(mask & obool)
        bad_hits     = np.sum(~mask & ebool) + np.sum(~mask & obool)
        missing_hits = np.sum(mask & ~ebool) + np.sum(mask & ~obool)

        points = [{"time": int(epoch_time)*S2NS, 'fields':{'nHits': evenhits+oddhits, 'good_hits': good_hits, 'bad_hits': bad_hits, 'missing_hits': missing_hits}, 'tags':{} },]
        return points
